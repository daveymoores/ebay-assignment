var cors = require("cors");
const express = require("express");
const compression = require("compression");
const expressStaticGzip = require("express-static-gzip");
const path = require('path');
const app = express().use(expressStaticGzip(__dirname + "/build/"));
app.use(compression());
const csv = require("csvtojson");
const http = require("http").Server(app);
const env = app.get("env");
let carJson;

if (env === "development") app.use(cors());

csv()
    .fromFile("vehicle_dataset.csv")
    .then(jsonArrayObj => {
        carJson = jsonArrayObj;
    });

app.get("/api/car-data", (req, res) => {
    res.set({ "Content-Type": "application/json" });
    res.send(JSON.stringify(carJson));
});

app.get("*", function(req, res) {
    const index = path.join(__dirname, 'build', 'index.html');
    res.sendFile(index);
});

const PORT = process.env.PORT || 3001;

http.listen(PORT, () => {
    console.log(`Server listen on port ${PORT}`);
});
