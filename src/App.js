// @flow

import React, { Component } from "react";
import styled from "styled-components";
import FormParent from "./Components/FormParent";
import CarSelect from "./Components/CarSelect";
import FetchCarData from "./fetchCarData";

const StyledContainer = styled.div`
    position: absolute;
    transform: translate3d(-50%, -50%, 0);
    top: 50%;
    left: 50%;
    width: 500px;
    min-height: 300px;
    margin: auto;
    background-color: #efefef;
    border-radius: 5px;
    border: 1px solid #ddd;
`;

const StyledSection = styled.div`
    width: 100%;
    padding: 25px;
    border-bottom: 1px solid #ddd;
    box-sizing: border-box;

    &:last-child {
        border-bottom: none;
    }
`;

const StyledTitle = styled.h3`
    font-size: 24px;
    margin: 0px;
    text-align: left;
`;

const StyledCarSelect = styled(CarSelect)`
    margin-bottom: 15px;
`;

const StyledButton = styled.button`
    width: 100%;
    padding: 10px;
    font-size: 15px;
    background-color: white;
    border-radius: 3px;
    pointer-events: ${props => (props.disabled ? `none` : `auto`)};
    opacity: ${props => (props.disabled ? 0.6 : 1)};
`;

const StyledInput = styled.input`
    margin-top: 15px;
    width: 100%;
    padding: 10px;
    font-size: 15px;
    box-sizing: border-box;
    border-radius: 3px;
    border: none;
`;

type State = {
    results: ?Array<Object>,
    btnDisabled: boolean,
    keywordsActive: boolean,
    inputValue: ?string,
    makeActive: boolean,
    modelActive: boolean,
    model: ?Object,
    make: ?Object
};

type Props = {};

/**
 * App fetches car data from api, generates selects and stores callback data
 * to pass back into select menus for filtering. Interaction with inputs and selects
 * triggers flags to be set that remove disabled prop on button when conditions are met
 * @extends Component
 */
class App extends Component<Props, State> {
    state = {
        results: null,
        keywordsActive: false,
        btnDisabled: true,
        inputValue: "",
        makeActive: false,
        modelActive: false,
        model: null,
        make: null
    };

    /**
     * fetches car data from api
     */
    componentDidMount = () => {
        const fetchCarData = new FetchCarData();
        fetchCarData.fetchData().then(res => {
            this.setState({ results: res });
        });
    };

    /**
     * checks whether update is a flag chang and if so, checks whether
     * button should be active
     */
    componentDidUpdate = (prevProps: Props, prevState: State) => {
        const { makeActive, modelActive, inputValue } = this.state;
        if (
            prevState.makeActive !== makeActive ||
            prevState.modelActive !== modelActive ||
            prevState.inputValue !== inputValue
        ) {
            this.checkAllFields();
        }
    };

    /**
     * takes callbacks from select menus
     */
    handleCallback = (value: string, label: string) => {
        const arr = [];
        arr.push(label);
        this.setState({
            [`${label}`]: value,
            [`${label}Active`]: true
        });
    };

    /**
     * handler for form input
     */
    handleInput = (e: SyntheticEvent<HTMLInputElement>) => {
        var target = e.target;
        if (target instanceof HTMLInputElement) {
            this.setState({ inputValue: target.value });
        }
    };

    handleClick = () => {
        alert("Searching for cars!");
    };

    /**
     * checks for null value on results, then creates an array that can be mapped over
     * to generate select menus based on columns in csv
     * @param  {Object} res results from api
     */
    returnSelectTypeArray = (res: ?Array<Object>) => {
        const arr: any = res ? Object.keys(res[0]) : null;
        return arr;
    };

    /**
     * sets isDisabled prop on select menu
     */
    checkDisabled = (makeActive: boolean, i: number) => {
        if (i) {
            if (makeActive) {
                return false;
            }
            return true;
        } else {
            return false;
        }
    };

    /**
     * performs check to see whether button can be made active based on
     * three flags that are associated with the input field and select menus
     */
    checkAllFields = () => {
        const { makeActive, modelActive, inputValue } = this.state;
        if (makeActive && modelActive && inputValue !== "") {
            this.setState({ btnDisabled: false });
        } else {
            this.setState({ btnDisabled: true });
        }
    };

    render() {
        const {
            results,
            btnDisabled,
            inputValue,
            make,
            makeActive
        } = this.state;

        return (
            <div className="App">
                <StyledContainer>
                    <StyledSection>
                        <StyledTitle>Buy a car</StyledTitle>
                    </StyledSection>
                    <StyledSection>
                        {results ? (
                            <FormParent>
                                {this.returnSelectTypeArray(results).map(
                                    (select, index) => {
                                        return (
                                            <StyledCarSelect
                                                key={`${index}_select`}
                                                callback={this.handleCallback}
                                                title={select}
                                                results={results}
                                                make={make}
                                                isDisabled={this.checkDisabled(
                                                    makeActive,
                                                    index
                                                )}
                                            />
                                        );
                                    }
                                )}
                            </FormParent>
                        ) : (
                            `Loading...`
                        )}
                        <StyledInput
                            type="text"
                            value={inputValue}
                            onChange={this.handleInput}
                            placeholder="Keywords..."
                        />
                    </StyledSection>
                    <StyledSection>
                        <StyledButton
                            disabled={btnDisabled}
                            type="submit"
                            onClick={this.handleClick}
                        >
                            Search Cars
                        </StyledButton>
                    </StyledSection>
                </StyledContainer>
            </div>
        );
    }
}

export default App;
