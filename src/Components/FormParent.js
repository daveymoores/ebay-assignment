// @flow

import * as React from "react";

type Props = {
    children?: React.Node
};

const FormParent = ({ children }: Props) => {
    return <form>{children}</form>;
};

export default FormParent;
