// @flow

import React, { Component } from "react";
import styled from "styled-components";
import Select from "react-select";

const StyledLabel = styled.label`
    font-size: 14px;
    text-align: left;
    padding-bottom: 15px;

    span {
        text-align: left;
        display: block;
        font-weight: bold;
        margin-bottom: 10px;
    }
`;

type Props = {
    callback: (x: ?string, y: ?string) => mixed,
    title: string,
    className: string,
    results: ?Array<Object>,
    make: ?Object & { label: ?string },
    isDisabled: boolean
};

type State = {
    selectedOption: ?string,
    selectedTitle: ?string,
    results: ?Array<Object>,
    filteredResults: ?Array<Object>
};

/**
 * The Select component takes results and a make prop, then filters it
 * to produce a model array that updates on the select onChange event
 */

class CarSelect extends Component<Props, State> {
    state = {
        selectedOption: null,
        selectedTitle: null,
        results: null,
        filteredResults: null
    };

    /**
     * on mount, the results are set and the array is
     * created in the setstate callback
     */
    componentDidMount = () => {
        const { title, results } = this.props;
        this.setState({ results }, () => {
            this.returnArrayOfType(title, results);
        });
    };

    /**
     * on update, the select checks whether it is the model select, then passes the
     * make through to the filter function and clears the select menu
     */
    componentDidUpdate = (prevProps: Props, prevState: State) => {
        const { make, title } = this.props;
        const { selectedOption } = this.state;
        if (prevProps.make !== make && title === "model" && selectedOption === prevState.selectedOption) {
            this.setState({selectedOption: null});
            this.updateOptions(make.label);
        }
    };

    /**
     * handleChange is called on the onchange event to pass state background
     * to the parent component as a callback, allowing the parent to know
     * which select has been activated and allowing the make to be passed
     * the model select.
     * @param  {Object} e
     */
    handleChange = (
        e: any
    ) => {
        const { title } = this.props;
        this.setState({ selectedOption: e, selectedTitle: title }, () => {
            this.props.callback(
                this.state.selectedOption,
                this.state.selectedTitle
            );
        });
    };

    /**
     * Once filtered, it returns an array of objects that populate the
     * select menu options and removes empty values, returning a filtered array
     * into state
     * @param  {string} type make/model
     * @param  {Object} res  results from state
     */
    returnArrayOfType = (type: string, res: ?Object) => {
        const arr = [];
        const uniquesArray = [];

        if (res) {
            for (const key in res) {
                if (res[key].hasOwnProperty(type)) {
                    if (
                        res[key][type] !== "" &&
                        uniquesArray.indexOf(res[key][type]) === -1
                    ) {
                        uniquesArray.push(res[key][type]);
                        arr.push({
                            value: res[key][type].toLowerCase(),
                            label: res[key][type]
                        });
                    }
                }
            }
        }

        this.setState({ filteredResults: arr });
    };

    /**
     * filters the results based on a filter term
     * @param  {string} filterTerm
     */
    updateOptions = (filterTerm: ?string) => {
        const { results } = this.state;
        const { title } = this.props;
        if(results) {
            const filtered = results.filter(item => item.make === filterTerm);
            this.returnArrayOfType(title, filtered);
        }
    };

    render() {
        const { title, className, isDisabled } = this.props;
        const { selectedOption, filteredResults } = this.state;

        return (
            <StyledLabel>
                <span>{`Choose a ${title}`}</span>
                {filteredResults ? (
                    <Select
                        value={selectedOption}
                        className={className}
                        onChange={this.handleChange}
                        options={filteredResults}
                        isDisabled={isDisabled}
                    />
                ) : null}
            </StyledLabel>
        );
    }
}

export default CarSelect;
