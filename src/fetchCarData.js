import axios from "axios";

export default class FetchCarData {
    fetchData = () => {
        const promise = new Promise((resolve, reject) => {
            axios
                .get("/api/car-data")
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    console.log(error);
                });
        });

        return promise;
    };
}
