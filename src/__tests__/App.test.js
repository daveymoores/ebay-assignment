import React from "react";
import { shallow, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import FetchCarData from "../fetchCarData";
import App from "../App";

configure({ adapter: new Adapter() });

jest.mock("../fetchCarData", () => {
    const users = { make: "RENAULT", model: "KANGOO" };
    return jest.fn().mockImplementation(() => {
        return {
            fetchData: function() {
                return Promise.resolve(users);
            }
        };
    });
});

describe("<App />", () => {
    it("App renders", () => {
        const tree = shallow(<App />);
        expect(tree).toMatchSnapshot();
    });

    it("App calls fetchCarData", () => {
        const wrapper = shallow(<App />);
        const instance = wrapper.instance();
        const spy = jest.spyOn(instance, "componentDidMount");
        wrapper.instance().componentDidMount();
        expect(spy).toHaveBeenCalledTimes(1);

        const fetchCarData = new FetchCarData();
        expect(fetchCarData.fetchData()).resolves.toEqual({
            make: "RENAULT",
            model: "KANGOO"
        });
    });
});
