import React from "react";
import { shallow, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import CarSelect from "../Components/CarSelect";

configure({ adapter: new Adapter() });

describe("<carSelect />", () => {
    const testData = {
        callback: jest.fn(),
        title: "title",
        results: { make: "RENAULT", model: "KANGOO" },
        make: null,
        isDisabled: false
    };

    it("select renders with test data", () => {
        const tree = shallow(<CarSelect {...testData} />);
        expect(tree).toMatchSnapshot();
    });

    it('calls updateOptions if make is passed into select menu', ()=>{
        const tree = shallow(<CarSelect {...testData} />);
        const instance = tree.instance();
        const spy = jest.spyOn(instance, "updateOptions");
        tree.setProps({make: "AUDI", title: "model"});
        tree.setState({results: {make: "AUDI", model: "A3"}});
        expect(spy).toHaveBeenCalledTimes(1);
        spy.mockRestore();
    });
});
