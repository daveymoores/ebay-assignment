// flow-typed signature: 4f00e29b8556cd4502f68c0ec87581f4
// flow-typed version: <<STUB>>/env_v^0.0.2/flow_v0.89.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'env'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'env' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module 'env/examples/db/index' {
  declare module.exports: any;
}

declare module 'env/examples/simple/index' {
  declare module.exports: any;
}

declare module 'env/lib/env' {
  declare module.exports: any;
}

declare module 'env/test/env.test' {
  declare module.exports: any;
}

// Filename aliases
declare module 'env/examples/db/index.js' {
  declare module.exports: $Exports<'env/examples/db/index'>;
}
declare module 'env/examples/simple/index.js' {
  declare module.exports: $Exports<'env/examples/simple/index'>;
}
declare module 'env/index' {
  declare module.exports: $Exports<'env'>;
}
declare module 'env/index.js' {
  declare module.exports: $Exports<'env'>;
}
declare module 'env/lib/env.js' {
  declare module.exports: $Exports<'env/lib/env'>;
}
declare module 'env/test/env.test.js' {
  declare module.exports: $Exports<'env/test/env.test'>;
}
