#### To run this project:

`yarn start`

#### How it works:

-   see flow.png
-   dev-server will run on port 3001
-   url is [here](https://ebay-djm-assignment.herokuapp.com/)
-   The data is stored in csv format which is served from the api using the npm package [csvtojson](https://www.npmjs.com/package/csvtojson)
-   When the app mounts it fetches the data from the api using [axios](https://www.npmjs.com/package/axios)
-   the number of select menus generated is determined by the number of columns in the csv
-   all the data is sent to the select menus and filtered by the type of select menu generated i.e. make / model
-   when the first select menu has changed, the value is passed to the next select menu and the data is filtered further so that only the corresponding models are shown
-   each time a value is selected from the select menus, or there is typing in the input field, a flag is set
-   if all the flags are set, the button is no longer disabled and can be clicked
